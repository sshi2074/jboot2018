package jboot.model.vo;

import java.io.Serializable;

public class PgColumnVo implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private int ordPosition;
	private String tabName;
	private String colName;
	private String dataType;

	public int getOrdPosition() {
		return ordPosition;
	}

	public void setOrdPosition(int ordPosition) {
		this.ordPosition = ordPosition;
	}

	public String getTabName() {
		return tabName;
	}

	public void setTabName(String tabName) {
		this.tabName = tabName;
	}

	public String getColName() {
		return colName;
	}

	public void setColName(String colName) {
		this.colName = colName;
	}

	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

}
