package jboot.model.jpa.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the eg_master database table.
 * 
 */
@Entity
@Table(name="eg_master")
@NamedQuery(name="EgMaster.findAll", query="SELECT e FROM EgMaster e")
public class EgMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="eg_master_id")
	private Integer egMasterId;

	@Temporal(TemporalType.DATE)
	@Column(name="col_date")
	private Date colDate;

	@Column(name="col_int")
	private Integer colInt;

	@Column(name="col_money")
	private BigDecimal colMoney;

	@Column(name="col_text")
	private String colText;

	@Column(name="col_timestamp")
	private Date colTimestamp;

	@Column(name="col_varchar")
	private String colVarchar;

	public EgMaster() {
	}

	public Integer getEgMasterId() {
		return this.egMasterId;
	}

	public void setEgMasterId(Integer egMasterId) {
		this.egMasterId = egMasterId;
	}

	public Date getColDate() {
		return this.colDate;
	}

	public void setColDate(Date colDate) {
		this.colDate = colDate;
	}

	public Integer getColInt() {
		return this.colInt;
	}

	public void setColInt(Integer colInt) {
		this.colInt = colInt;
	}

	public BigDecimal getColMoney() {
		return colMoney;
	}

	public void setColMoney(BigDecimal colMoney) {
		this.colMoney = colMoney;
	}

	public String getColText() {
		return this.colText;
	}

	public void setColText(String colText) {
		this.colText = colText;
	}

	public Date getColTimestamp() {
		return this.colTimestamp;
	}

	public void setColTimestamp(Date colTimestamp) {
		this.colTimestamp = colTimestamp;
	}

	public String getColVarchar() {
		return this.colVarchar;
	}

	public void setColVarchar(String colVarchar) {
		this.colVarchar = colVarchar;
	}

}