package jboot.model.jpa.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the tm_object database table.
 * 
 */
@Entity
@Table(name="tm_object")
@NamedQuery(name="TmObject.findAll", query="SELECT t FROM TmObject t")
public class TmObject implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="tm_object_id")
	private Integer tmObjectId;

	@Column(name="base_table")
	private String baseTable;

	@Column(name="created_by")
	private String createdBy;

	@Column(name="date_created")
	private Date dateCreated;

	@Column(name="date_updated")
	private Date dateUpdated;

	private String note;

	@Column(name="object_name")
	private String objectName;

	private String status;

	@Column(name="updated_by")
	private String updatedBy;

	public TmObject() {
	}

	public Integer getTmObjectId() {
		return this.tmObjectId;
	}

	public void setTmObjectId(Integer tmObjectId) {
		this.tmObjectId = tmObjectId;
	}

	public String getBaseTable() {
		return this.baseTable;
	}

	public void setBaseTable(String baseTable) {
		this.baseTable = baseTable;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getDateCreated() {
		return this.dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getDateUpdated() {
		return this.dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	public String getNote() {
		return this.note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getObjectName() {
		return this.objectName;
	}

	public void setObjectName(String objectName) {
		this.objectName = objectName;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

}