package jboot.model.jpa.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the tm_list database table.
 * 
 */
@Entity
@Table(name="tm_list")
@NamedQuery(name="TmList.findAll", query="SELECT t FROM TmList t")
public class TmList implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="tm_list_id")
	private Integer tmListId;

	private String cmd;

	@Column(name="cmd_key_yn")
	private String cmdKeyYn;

	@Column(name="column_name")
	private String columnName;

	@Column(name="column_type")
	private String columnType;

	@Column(name="created_by")
	private String createdBy;

	@Column(name="date_created")
	private Date dateCreated;

	@Column(name="date_updated")
	private Date dateUpdated;

	@Column(name="disp_opt_width")
	private String dispOptWidth;

	@Column(name="disp_order_no")
	private Integer dispOrderNo;

	@Column(name="disp_text")
	private String dispText;

	private String note;

	@Column(name="srch_format")
	private String srchFormat;

	@Column(name="srch_yn")
	private String srchYn;

	private String status;

	@Column(name="table_name")
	private String tableName;

	@Column(name="tm_object_id")
	private Integer tmObjectId;

	@Column(name="updated_by")
	private String updatedBy;

	public TmList() {
	}

	public Integer getTmListId() {
		return this.tmListId;
	}

	public void setTmListId(Integer tmListId) {
		this.tmListId = tmListId;
	}

	public String getCmd() {
		return this.cmd;
	}

	public void setCmd(String cmd) {
		this.cmd = cmd;
	}

	public String getCmdKeyYn() {
		return this.cmdKeyYn;
	}

	public void setCmdKeyYn(String cmdKeyYn) {
		this.cmdKeyYn = cmdKeyYn;
	}

	public String getColumnName() {
		return this.columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	public String getColumnType() {
		return this.columnType;
	}

	public void setColumnType(String columnType) {
		this.columnType = columnType;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getDateCreated() {
		return this.dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getDateUpdated() {
		return this.dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	public String getDispOptWidth() {
		return this.dispOptWidth;
	}

	public void setDispOptWidth(String dispOptWidth) {
		this.dispOptWidth = dispOptWidth;
	}

	public Integer getDispOrderNo() {
		return this.dispOrderNo;
	}

	public void setDispOrderNo(Integer dispOrderNo) {
		this.dispOrderNo = dispOrderNo;
	}

	public String getDispText() {
		return this.dispText;
	}

	public void setDispText(String dispText) {
		this.dispText = dispText;
	}

	public String getNote() {
		return this.note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getSrchFormat() {
		return this.srchFormat;
	}

	public void setSrchFormat(String srchFormat) {
		this.srchFormat = srchFormat;
	}

	public String getSrchYn() {
		return this.srchYn;
	}

	public void setSrchYn(String srchYn) {
		this.srchYn = srchYn;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTableName() {
		return this.tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public Integer getTmObjectId() {
		return this.tmObjectId;
	}

	public void setTmObjectId(Integer tmObjectId) {
		this.tmObjectId = tmObjectId;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

}