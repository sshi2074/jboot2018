package jboot.model.jpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import jboot.model.jpa.entity.EgMaster;

public interface EgMasterRepository extends JpaRepository<EgMaster,Integer> {

}
