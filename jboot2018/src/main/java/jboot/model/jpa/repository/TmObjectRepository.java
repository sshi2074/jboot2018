package jboot.model.jpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import jboot.model.jpa.entity.TmObject;

public interface TmObjectRepository extends JpaRepository<TmObject,Integer> {

}
