package jboot.model.jpa.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import jboot.model.jpa.entity.TmList;

public interface TmListRepository extends JpaRepository<TmList,Integer> {
	
	List<TmList> findByTmObjectId(Integer argObjId);
	List<TmList> findByTmObjectIdOrderByDispOrderNo(Integer argObjId);
		
	@Query("SELECT t FROM TmList t WHERE t.tmObjectId = :objId AND t.srchYn='Y' ORDER BY t.dispOrderNo")
	List<TmList> findListFormCols(@Param("objId") Integer argObjId);
	
	@Query("SELECT t FROM TmList t WHERE t.tmObjectId = :objId AND (t.cmdKeyYn != 'Y' OR t.cmdKeyYn IS NULL) ORDER BY t.dispOrderNo")
	List<TmList> findEditFormCols(@Param("objId") Integer argObjId);
	
	TmList findByTmObjectIdAndCmdKeyYn(Integer argObjId,String argKeyYn);		
}
