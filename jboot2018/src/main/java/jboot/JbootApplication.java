package jboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JbootApplication {

	public static void main(String[] args) {
		SpringApplication.run(JbootApplication.class, args);
	}
}
