package jboot.controller;

import java.util.Enumeration;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jboot.service.impl.JmateCustService;
import jboot.service.impl.PgService;

@Controller
public class JmateSvcController {
	
	@Autowired
	JmateCustService svcJmateCust;
	
	@Autowired
	PgService svcPosgres;
	
	private static final Logger logger = LoggerFactory.getLogger(JmateSvcController.class);
	
	@RequestMapping(value = "/jm/svc/list/{objId}", method = RequestMethod.GET)
	public String getListSrchForm(@PathVariable("objId") Integer argObjId, Locale locale, Model model) throws Exception {
		
		model.addAttribute("maObjId", argObjId);				
		model.addAttribute("maListCols", svcJmateCust.getListFormCols(argObjId));
		model.addAttribute("maListMapResultSet", svcJmateCust.getListResultSet(argObjId));
		model.addAttribute("maKeyCol", svcJmateCust.getKeyCol(argObjId));
		
		return "jmate/svc/list";
	}
	
	@RequestMapping(value = "/jm/svc/list/{objId}/srch", method = RequestMethod.GET)
	public String getListSrch(@PathVariable("objId") Integer argObjId, HttpServletRequest req, Locale locale, Model model) throws Exception {
		
		model.addAttribute("maObjId", argObjId);
		model.addAttribute("maListCols", svcJmateCust.getListFormCols(argObjId));
		model.addAttribute("maListMapResultSet", svcJmateCust.getListSrchResultSet(argObjId,req));
		model.addAttribute("maKeyCol", svcJmateCust.getKeyCol(argObjId));
		
		return "jmate/svc/list";
	}	
	
	
	@RequestMapping(value = "/jm/svc/obj/{objId}/edit/{itemId}", method = RequestMethod.GET)
	public String getEditForm(@PathVariable("objId") Integer argObjId,@PathVariable("itemId") Integer argItemId, Locale locale, Model model) throws Exception {

		model.addAttribute("maObjId", argObjId);
		model.addAttribute("maEditCols", svcJmateCust.getEditFormCols(argObjId));
		
		if(argItemId!=null && argItemId>0) {
			
			model.addAttribute("maMapResutlSet", svcJmateCust.getSingleResultSet(argObjId, argItemId));
			return "jmate/svc/edit";
			
		} else {
			
			return "jmate/svc/new";
		}
	}
	
	@Transactional
	@RequestMapping(value = "/jm/svc/obj/{objId}/edit/{itemId}", method = RequestMethod.POST)
	public String setEditForm(@PathVariable("objId") Integer argObjId,@PathVariable("itemId") Integer argItemId, HttpServletRequest req, Locale locale, Model model) throws Exception {

		 Enumeration eParam = req.getParameterNames();
		 while (eParam.hasMoreElements()) {
			 String pName = (String)eParam.nextElement();
			 String pValue = req.getParameter(pName);

			 logger.debug("# name, value => " + pName + ":" + pValue);
		 }
		
		svcJmateCust.setEditFormCols(argObjId, argItemId, req);
		
		return "redirect:/jm/svc/list/"+argObjId;		
	}
	

}
