package jboot.controller;

import java.util.Locale;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jboot.model.jpa.entity.TmList;
import jboot.model.jpa.entity.TmObject;
import jboot.service.impl.JmateService;
import jboot.service.impl.PgService;

@Controller
public class JmateToolController {
	
	@Autowired
	JmateService svcJmate;
	
	@Autowired
	PgService svcPosgres;
	
	private static final Logger logger = LoggerFactory.getLogger(JmateToolController.class);
	
	@RequestMapping(value = "/jmate/object/list", method = RequestMethod.GET)
	public String getListExample(Locale locale, Model model) throws Exception {
		model.addAttribute("maListObject", svcJmate.getListObject());
		return "jmate/objList";
	}
	
	@RequestMapping(value = "/jmate/object/edit/{id}", method = RequestMethod.GET) 
	public String getObjectEditForm(@PathVariable("id") Integer argId, Locale locale, Model model) throws Exception {
		
		if(argId > 0) {
			model.addAttribute("maObject", svcJmate.getObjectById(argId));
			
		} else {
			TmObject tmObject = new TmObject();
			tmObject.setTmObjectId(0);
			
			model.addAttribute("maObject", tmObject);
		}
		
		return "jmate/objEdit";		
	}
	
	@Transactional
	@RequestMapping(value = "/jmate/object/edit/{id}", method = RequestMethod.POST)
	public String setObjectEditForm(@PathVariable("id") Integer argId, @Valid @ModelAttribute("maObject") TmObject maObject, BindingResult result, Locale locale, Model model) throws Exception {
		
        if (result.hasErrors()) {
        	logger.debug("# input validation error - count: " + result.getErrorCount() + " , error field name:" + result.getObjectName());        	        	
            return "jmate/objEdit";
        }    
        
        svcJmate.setObject(argId, maObject);
       
		return "redirect:/jmate/object/list";
	}
	
	@RequestMapping(value="/jmate/srchlist/obj/{objId}/item/{itemId}", method=RequestMethod.GET) 
	public String getSrchListForm(@PathVariable("objId") Integer argObjId, @PathVariable("itemId") Integer argItemId, Locale locale, Model model) throws Exception {
		
		TmObject tmObject = svcJmate.getObjectById(argObjId);
		
		model.addAttribute("maObject", tmObject);
		model.addAttribute("maPgColumn", svcPosgres.getPgColInfoByTblNm(tmObject.getBaseTable()));
		model.addAttribute("maSrchList", svcJmate.getTmListByObjId(argObjId));
		model.addAttribute("maSrchListItem", svcJmate.getTmListItemByIdOrNull(argObjId, argItemId));
						
		return "jmate/srchList";
	}

	@RequestMapping(value="/jmate/srchlist/obj/{objId}/item/{itemId}/edit", method=RequestMethod.GET) 
	public String getSrchListItemForm(@PathVariable("objId") Integer argObjId,@PathVariable("itemId") Integer argItemId,  Locale locale, Model model) throws Exception {
		
		TmObject tmObject = svcJmate.getObjectById(argObjId);
		
		model.addAttribute("maObject", tmObject);
		model.addAttribute("maPgColumn", svcPosgres.getPgColInfoByTblNm(tmObject.getBaseTable()));
		model.addAttribute("maSrchList", svcJmate.getTmListByObjId(argObjId));
		model.addAttribute("maSrchListItem", svcJmate.getTmListItemById(argItemId));
		
		return "jmate/srchListEdit";
	}
	
	@RequestMapping(value="/jmate/srchlist/obj/{objId}/item/{itemId}/edit", method=RequestMethod.POST)
	public String setSrchListItemForm(@PathVariable("objId") Integer argObjId,
									  @PathVariable("itemId") Integer argItemId,  
									  @Valid @ModelAttribute("maSrchListItem") TmList maSrchListItem, BindingResult result,
									  Locale locale, Model model) throws Exception {
		
        if (result.hasErrors()) {
        	logger.debug("# input validation error - count: " + result.getErrorCount() + " , error field name:" + result.getObjectName());        	        	
            return "jmate/srchListEdit";
        }    
		
		svcJmate.setTmListItem(argItemId, maSrchListItem);
				
		return "redirect:/jmate/srchlist/obj/"+argObjId+"/item/" + argItemId;
	}
			
	@Transactional
	@RequestMapping(value="/jmate/srchlist/initload/obj/{id}", method=RequestMethod.GET) 
	public String setBaseTabToSrchList(@PathVariable("id") Integer argObjId, Locale locale, Model model) throws Exception {
		
		svcJmate.setBaseTabToSrchList(argObjId);
		
		return "redirect:/jmate/srchlist/obj/" + argObjId;
	}

}
