package jboot.controller;

import java.util.Locale;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jboot.model.jpa.entity.EgMaster;
import jboot.service.impl.PgEgService;

@Controller
public class PostgresController {

	@Autowired
	PgEgService svcPgEg;
	
	private static final Logger logger = LoggerFactory.getLogger(PostgresController.class);
	
	@RequestMapping(value = "/pg/example/list", method = RequestMethod.GET)
	public String getListExample(Locale locale, Model model) throws Exception {
		
		logger.debug("# /pg/example/list: started -");
		logger.debug("# locale: {} ", locale);

		model.addAttribute("maList", svcPgEg.getListEgMaster());
		
		return "postgres/egList";
	}
	
	@RequestMapping(value = "/pg/example/{id}", method = RequestMethod.GET)
	public String getExample(@PathVariable("id") Integer argId, Locale locale, Model model) throws Exception {
		
		if(argId > 0) {
			model.addAttribute("maExample", svcPgEg.getEgMasterById(argId));
			
		} else {
			EgMaster egMaster = new EgMaster();
			egMaster.setEgMasterId(0);
			
			model.addAttribute("maExample", egMaster);
		}
				
		return "postgres/egDetail";
	}
	
	@RequestMapping(value="/pg/example/edit/{id}", method = RequestMethod.GET)
	public String getEditForm(@PathVariable("id") Integer argId, Locale locale, Model model) throws Exception {

		if(argId > 0) {
			model.addAttribute("maExample", svcPgEg.getEgMasterById(argId));
			
		} else {
			EgMaster egMaster = new EgMaster();
			egMaster.setEgMasterId(0);
			
			model.addAttribute("maExample", egMaster);
		}
				
		return "postgres/egEdit";
	}

	@RequestMapping(value="/pg/example/edit/{id}", method = RequestMethod.POST)
	public String setEditForm(@PathVariable("id") Integer argId,
							  @Valid @ModelAttribute("maExample") EgMaster maExample, BindingResult result,
							  Locale locale, Model model) throws Exception {

        if (result.hasErrors()) {
        	logger.debug("# input validation error - count: " + result.getErrorCount() + " / objectname:" + result.getObjectName());        	        	
            return "postgres/egEdit";
        }    		
		
        EgMaster egMaster = svcPgEg.setEgMaster(argId, maExample);
        
		return "redirect:/pg/example/"+egMaster.getEgMasterId();
	}

}
