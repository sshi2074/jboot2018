package jboot.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import jboot.model.vo.PgColumnVo;

@Service
public class PgService {
	
	@PersistenceContext
	EntityManager em;
	
    @Autowired
    Environment env;	
	
	private static final Logger logger = LoggerFactory.getLogger(PgService.class);
	
	public List<PgColumnVo> getPgColInfoByTblNm(String argTabName) throws Exception {
		
		List<PgColumnVo> list = new ArrayList<PgColumnVo>();
		
		String strSqlStmt = " SELECT "
						  + "    ordinal_position, table_name, column_name, data_type " 
						  + " FROM INFORMATION_SCHEMA.COLUMNS "
						  + " WHERE "  
						  + "     table_catalog = :dbName AND table_name = :tabName " 
						  + " ORDER BY ordinal_position ";
		
		String strDbName = env.getProperty("custom.database.name");
		logger.debug("# dbName: " + strDbName);
		
		List<Object[]> listResutlSet = (List<Object[]>)em.createNativeQuery(strSqlStmt).setParameter("dbName",strDbName).setParameter("tabName",argTabName).getResultList();
		
		logger.debug("# listResutlSet.size() " + listResutlSet.size());
		
		for(Object[] obj : listResutlSet) {			
			PgColumnVo c = new PgColumnVo();
			c.setOrdPosition((int)obj[0]);
			c.setTabName((String)obj[1]);
			c.setColName((String)obj[2]);
			c.setDataType((String)obj[3]);
			
			list.add(c);			
		}
				
		return list;
	}
		
}
