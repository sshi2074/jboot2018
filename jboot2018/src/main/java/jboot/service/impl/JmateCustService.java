package jboot.service.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;

import jboot.model.jpa.entity.TmList;
import jboot.model.jpa.repository.TmListRepository;

@Service
public class JmateCustService {
		
	@Autowired
	TmListRepository repoTmList;
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@Autowired
	MyJdbcService svcJdbc;
	
	private static final Logger logger = LoggerFactory.getLogger(JmateCustService.class);

	public List<TmList> getListFormCols(Integer argObjId) throws Exception {
		return repoTmList.findListFormCols(argObjId);
	}
	
	public TmList getKeyCol(Integer argObjId) throws Exception {
		return repoTmList.findByTmObjectIdAndCmdKeyYn(argObjId, "Y");
	}
	
	public List<Map<String,Object>> getListResultSet(Integer argObjId) throws Exception {
				
		String strSqlStmt = makeSqlSelectStmt(argObjId);
		
		TmList tmList = getKeyCol(argObjId);
		String strStmtOrder = " ORDER BY " + tmList.getColumnName() + " DESC ";
		logger.debug("# sql select stmt: " + strSqlStmt + strStmtOrder);
		
		List<Map<String,Object>> listMapResultSet = jdbcTemplate.queryForList(strSqlStmt + strStmtOrder);
						
		return listMapResultSet;		
	}
	
	public List<Map<String,Object>> getListSrchResultSet(Integer argObjId, HttpServletRequest argHttpReq) throws Exception {
				
		List<TmList> listSrchCol = repoTmList.findListFormCols(argObjId);
		
		String strSqlStmt = makeSqlSelectStmt(argObjId);
		logger.debug("# sql select stmt: " + strSqlStmt);
		
				
		String strSqlWhereStmt = makeSqlSelectParamQryStmt(argObjId,argHttpReq);
		if(strSqlWhereStmt.equals("")) {
			return getListResultSet(argObjId);
		}
		
		TmList tmList = getKeyCol(argObjId);
		String strStmtOrder = " ORDER BY " + tmList.getColumnName() + " DESC ";
		
		logger.debug("# sql select where: " + strSqlWhereStmt);
		logger.debug("# sql stmt: " + strSqlStmt + strSqlWhereStmt);
		
		
		int nCount = 0;
		Map<Integer,Object> mapParam = new LinkedHashMap<Integer,Object>();
				
        for(TmList c:listSrchCol) {                		                	
        	String strReqVal = (String)argHttpReq.getParameter(c.getColumnName());
        	if(strReqVal!=null && !strReqVal.equals("")) {
        		nCount++;
            	if(c.getColumnType().startsWith("character varying") || c.getColumnType().startsWith("text")) {
            		mapParam.put(nCount,strReqVal);
            	} else if(c.getColumnType().startsWith("int")) {
            		mapParam.put(nCount,Integer.parseInt(strReqVal));
            	}
        	}  	                	
        }		
		
        List<Map<String, Object>> listMapResultSet = svcJdbc.getResultList(strSqlStmt + strSqlWhereStmt + strStmtOrder, mapParam);
		
		return listMapResultSet;
	}
	
	public Map<String,Object> getSingleResultSet(Integer argObjId, Integer argKeyId) throws Exception {
		
		TmList tmList = getKeyCol(argObjId);
		
		String strSqlStmt = " SELECT * FROM " + tmList.getTableName() + " WHERE " + tmList.getColumnName() + " = ? ";
		Map<Integer,Object> mapParam = new LinkedHashMap<Integer,Object>();
		mapParam.put(1, argKeyId);
		
		List<Map<String,Object>> listMapResultSet= svcJdbc.getResultList(strSqlStmt, mapParam);
		
		return listMapResultSet.get(0);
	}
		
	public List<TmList> getEditFormCols(Integer argObjId) throws Exception {
		return repoTmList.findEditFormCols(argObjId);
	}
	
	public void setEditFormCols(Integer argObjId, Integer argItemId, HttpServletRequest argReq) throws Exception {
		
		if(argItemId==0) {
			dbInsert(argObjId,argItemId,argReq);
		} else {
			dbUpdate(argObjId,argItemId,argReq);			
		}
		
	}
		
	private void dbUpdate(Integer argObjId, Integer argItemId, HttpServletRequest argReq) throws Exception {
		
		String strSqlStmt = makeSqlUpdateStmt(argObjId, argReq);
		logger.debug("# sql update stmt: " + strSqlStmt);
		
		if(strSqlStmt==null || strSqlStmt.equals("")) {
			logger.info("# sql error: stop");
			return;
		}
		
		List<TmList> listDbEditCols = repoTmList.findEditFormCols(argObjId);
		Map<Integer,Object> mapParam = new LinkedHashMap<Integer,Object>(); 
		
		int nCount = 0;
		for(TmList c: listDbEditCols) {
			String strReqVal = (String)argReq.getParameter(c.getColumnName());			
			if(strReqVal!=null && !strReqVal.equals("")) {
				mapParam.put(++nCount, strReqVal); 
			}			
		}		
		mapParam.put(++nCount, argItemId);
		
		int nUpdateCount = svcJdbc.execUpdate(strSqlStmt, mapParam);
		logger.debug("# nUpdateCount: " + nUpdateCount);
		
	}
		
	private void dbInsert(Integer argObjId, Integer argItemId, HttpServletRequest argReq) throws Exception {
		
		String strSqlStmt = makeSqlInsertStmt(argObjId);
		logger.debug("# sql insert stmt: " + strSqlStmt);
		
		if(strSqlStmt==null || strSqlStmt.equals("")) {
			logger.info("# sql error: stop");
			return;
		}
		
		List<TmList> listDbEditCols = repoTmList.findEditFormCols(argObjId);
		
		KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                PreparedStatement ps = connection.prepareStatement(strSqlStmt, Statement.RETURN_GENERATED_KEYS);
                
                int nCount=0;
                for(TmList c:listDbEditCols) {                	
                	++nCount;
                	String strReqVal = (String)argReq.getParameter(c.getColumnName());
                	
                	if(c.getColumnType().startsWith("character varying")) {
                		ps.setString(nCount, strReqVal);
                	} else {
                		ps.setObject(nCount, null);
                	}                	                	
                }
                return ps;
            }
        }, keyHolder);
		
        //logger.debug("# new PK id: " + keyHolder.getKey().intValue());		
	}
	
	private String makeSqlSelectStmt(Integer argObjId) throws Exception {
		
		String strSqlStmtFull = "";
		
		List<TmList> listDbCols = repoTmList.findListFormCols(argObjId);
		
		if(listDbCols.size() < 1) {
			logger.debug("# error - listDbCols.size(): " + listDbCols.size());			
			return null;
		}
		
		String strSqlStmtHead = " SELECT ";
		String strSqlStmtCols = "";
		String strSqlStmtWhere = "";
		String strSqlStmtOrder = "";
		
		for(TmList c : listDbCols) {
			strSqlStmtCols += ", " + c.getColumnName();
		}
		strSqlStmtCols = strSqlStmtCols.substring(1);		
		
		strSqlStmtFull = strSqlStmtHead + strSqlStmtCols + " FROM " + listDbCols.get(0).getTableName();
		
		return strSqlStmtFull;
	}
	
	private String makeSqlSelectParamQryStmt(Integer argObjId, HttpServletRequest argHttpReq) throws Exception {
		
		String strWhereStmt = "";
		
		List<TmList> listDbEditCols = repoTmList.findListFormCols(argObjId);
		
		String strSqlParam = "";
		for(TmList c : listDbEditCols) {			
			String strReqVal = (String)argHttpReq.getParameter(c.getColumnName());			
			if(strReqVal!=null && !strReqVal.equals("")) {
				strSqlParam += "AND " +  c.getColumnName() + " = ? "; 
			}
		}		
		
		if(strSqlParam.equals("")) {
			strWhereStmt = "";
		} else {
			strSqlParam = strSqlParam.substring(3);
			strWhereStmt = " WHERE " + strSqlParam; 					
		}
		
		return strWhereStmt;
	}
	
	private String makeSqlInsertStmt(Integer argObjId) throws Exception {
		
		String strSqlStmtFull = "";
		
		List<TmList> listDbEditCols = repoTmList.findEditFormCols(argObjId);
		
		if(listDbEditCols.size() < 1) {
			logger.debug("# error - listDbEditCols.size(): " + listDbEditCols.size());			
			return null;
		}
		
		String strSqlStmtHead = "INSERT INTO " + listDbEditCols.get(0).getTableName();
		String strSqlStmtCols = "";
		String strSqlStmtQues = "";
				
		for(TmList c : listDbEditCols) {	
						
			strSqlStmtCols += ", " + c.getColumnName();
			strSqlStmtQues += ", ?";
		}	
		
		strSqlStmtCols = strSqlStmtCols.substring(1);
		strSqlStmtQues = strSqlStmtQues.substring(1);
				
		strSqlStmtFull = strSqlStmtHead + " ( " + strSqlStmtCols + " ) VALUES ( " + strSqlStmtQues + " ) "; 
		
		return strSqlStmtFull;		
	}
		
	private String makeSqlUpdateStmt(Integer argObjId, HttpServletRequest argReq) throws Exception {
		
		List<TmList> listDbEditCols = repoTmList.findEditFormCols(argObjId);
		TmList tmList = getKeyCol(argObjId);
		
		String strSqlStmtFull = "";
		
		String strSqlStmtHead = " UPDATE " + tmList.getTableName() + " SET ";
		String strSqlStmtWhere = " WHERE " + tmList.getColumnName() + " = ? ";
		
		String strSqlParam = "";
		for(TmList c: listDbEditCols) {
			String strReqVal = (String)argReq.getParameter(c.getColumnName());			
			if(strReqVal!=null && !strReqVal.equals("")) {
				strSqlParam += "," +  c.getColumnName() + " = ? "; 
			}			
		}
		
		if(strSqlParam.length()>1) {

			strSqlParam = strSqlParam.substring(1);
			
			strSqlStmtFull = strSqlStmtHead + strSqlParam + strSqlStmtWhere;
						
		} else {
			strSqlStmtFull = null;
		}
		
		return strSqlStmtFull;
	}
}
