package jboot.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jboot.model.jpa.entity.EgMaster;
import jboot.model.jpa.repository.EgMasterRepository;

@Service
public class PgEgService {
	
	@Autowired
	EgMasterRepository repoEgMaster;
	
	public List<EgMaster> getListEgMaster() throws Exception {
		return repoEgMaster.findAll();
	}
	
	public EgMaster getEgMasterById(Integer argId) throws Exception {
		return repoEgMaster.findOne(argId);
	}
	
	public EgMaster setEgMaster(Integer argId, EgMaster argEgMaster) throws Exception {
		
		EgMaster egMaster = null;
		
		if( argId != null && argId > 0 ) {
			egMaster = updateJob(argId, argEgMaster);
			
		} else {
			egMaster = insertJob(argEgMaster);
		}
		
		return egMaster;
	}
	
	private EgMaster insertJob(EgMaster argEgMaster) throws Exception {
		
		argEgMaster.setEgMasterId(null);
		argEgMaster.setColDate(new Date());
		argEgMaster.setColTimestamp(new Date());
		
		return repoEgMaster.saveAndFlush(argEgMaster);		
	}
	
	
	private EgMaster updateJob(Integer argId, EgMaster argEgMaster) throws Exception {

		EgMaster egMaster = repoEgMaster.findOne(argId);
		
		if(egMaster!=null) {
			
			egMaster.setColInt(argEgMaster.getColInt());
			egMaster.setColMoney(argEgMaster.getColMoney());
			egMaster.setColVarchar(argEgMaster.getColVarchar());
			egMaster.setColText(argEgMaster.getColText());
			egMaster.setColDate(new Date());
			egMaster.setColTimestamp(new Date());		
			
			egMaster = repoEgMaster.saveAndFlush(egMaster);
		} 
		
		return egMaster;		
	}
}
