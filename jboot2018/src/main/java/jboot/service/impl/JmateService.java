package jboot.service.impl;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jboot.model.jpa.entity.TmList;
import jboot.model.jpa.entity.TmObject;
import jboot.model.jpa.repository.TmListRepository;
import jboot.model.jpa.repository.TmObjectRepository;
import jboot.model.vo.PgColumnVo;

@Service
public class JmateService {
	
	@Autowired
	TmObjectRepository repoTmObject;
	
	@Autowired
	PgService svcPostgres;
	
	@Autowired
	TmListRepository repoTmList;
	
	private static final Logger logger = LoggerFactory.getLogger(JmateService.class);

	public List<TmObject> getListObject() throws Exception {
		return repoTmObject.findAll();
	}
		
	public TmObject getObjectById(Integer argId) throws Exception {
		return repoTmObject.findOne(argId);
	}
	
	public TmObject setObject(Integer argId, TmObject argObj) throws Exception {
		
		TmObject dbObj = null;
		
		if( argId != null && argId > 0 ) {
			
			dbObj = repoTmObject.findOne(argId);
			
			dbObj.setObjectName(argObj.getObjectName());
			dbObj.setBaseTable(argObj.getBaseTable());

			dbObj.setNote(argObj.getNote());
			dbObj.setStatus(argObj.getStatus());			
			//dbObj.setCreatedBy();
			//dbObj.setDateCreated();
			dbObj.setUpdatedBy("INIT:TESTER");
			dbObj.setDateUpdated(new Date());
			
			dbObj = repoTmObject.saveAndFlush(dbObj);
			
		} else {
			
			argObj.setTmObjectId(null);
			
			argObj.setCreatedBy("INIT:TESTER");
			argObj.setDateCreated(new Date());
			argObj.setUpdatedBy("INIT:TESTER");
			argObj.setDateUpdated(new Date());
			
			dbObj = repoTmObject.saveAndFlush(argObj);
		}
		
		return dbObj;
	}	
	
	public void setBaseTabToSrchList(Integer argObjId) throws Exception {
		
		TmObject tmObject = repoTmObject.findOne(argObjId);
		
		/* delete prior records to init */
		List<TmList> listTmList = repoTmList.findByTmObjectId(tmObject.getTmObjectId());		
		repoTmList.deleteInBatch(listTmList);		
		repoTmList.flush();
				
		List<PgColumnVo> listPgCol = svcPostgres.getPgColInfoByTblNm(tmObject.getBaseTable());
		for(PgColumnVo c:listPgCol) {
			TmList l = new TmList();
			
			l.setTmListId(null);
			l.setTmObjectId(tmObject.getTmObjectId());
			l.setTableName(c.getTabName());
			l.setColumnName(c.getColName());
			l.setColumnType(c.getDataType());
			l.setDispText(c.getColName());
			l.setDispOrderNo(c.getOrdPosition());
			
			l.setCreatedBy("init:tester");
			l.setDateCreated(new Date());
			l.setUpdatedBy("init:tester");
			l.setDateUpdated(new Date());
			
			repoTmList.saveAndFlush(l);
		}
	}
	
	public List<TmList> getTmListByObjId(Integer argObjId) throws Exception {
		return repoTmList.findByTmObjectIdOrderByDispOrderNo(argObjId);
	}
	
	public TmList getTmListItemById(Integer argId) throws Exception {
		return repoTmList.findOne(argId);
	}

	public TmList getTmListItemByIdOrNull(Integer argObjId, Integer argId) throws Exception {
		
		if(argId==null || argId==0) {
			
			List<TmList> list = repoTmList.findByTmObjectIdOrderByDispOrderNo(argObjId);
			if(list.size()>0) {
				return list.get(0);
			} else {
				return null;
			}
			
		} else {
			return repoTmList.findOne(argId);
		}
		
	}
	
	
	public TmList setTmListItem(Integer argId, TmList argTmList) throws Exception {
		
		TmList dbItem = repoTmList.findOne(argId);
		
		dbItem.setCmd(argTmList.getCmd());
		dbItem.setCmdKeyYn(argTmList.getCmdKeyYn());
		dbItem.setColumnName(argTmList.getColumnName());
		dbItem.setColumnType(argTmList.getColumnType());		
		
		dbItem.setCreatedBy("INIT:TESTER");		
		dbItem.setDateCreated(new Date());
		dbItem.setDateUpdated(new Date());
		dbItem.setDispOptWidth(argTmList.getDispOptWidth());
		
		dbItem.setDispOrderNo(argTmList.getDispOrderNo());
		dbItem.setDispText(argTmList.getDispText());
		
		//10
		
		dbItem.setNote(argTmList.getNote());		
		dbItem.setSrchFormat(argTmList.getSrchFormat());
		dbItem.setSrchYn(argTmList.getSrchYn());
		dbItem.setStatus(argTmList.getStatus());
		
		dbItem.setTableName(argTmList.getTableName());		
		dbItem.setTmListId(argTmList.getTmListId());
		dbItem.setTmObjectId(argTmList.getTmObjectId());
		dbItem.setUpdatedBy("INIT:TESTER");
		
		//18
		
		return repoTmList.saveAndFlush(dbItem);
	}
}
