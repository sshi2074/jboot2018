package jboot.service.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class MyJdbcService {
	
	@Resource
	DataSource ds;		
	
	private static final Logger logger = LoggerFactory.getLogger(MyJdbcService.class);

	public List<Map<String,Object>> getResultList(String strSqlStmt) {
		
		Connection conn = null;
		List<Map<String,Object>> rsListRet = new ArrayList<Map<String,Object>>();
				
		try {
			conn = ds.getConnection();
			
			PreparedStatement prepStmt = conn.prepareStatement(strSqlStmt);			
			ResultSet rs = prepStmt.executeQuery();
			ResultSetMetaData rsmd = rs.getMetaData();
			
			while(rs.next()) {
				
				HashMap<String,Object> map = new LinkedHashMap<String,Object>();
								
				for (int i = 1; i <= rsmd.getColumnCount(); i++) {										
					map.put(rsmd.getColumnName(i), rs.getObject(i));
					logger.debug("# rsmd: " + rsmd.getColumnName(i) + ": [ " + rs.getObject(i) + " ]");
				}
				rsListRet.add(map);								
			}												
		} catch(Exception e) {
			e.printStackTrace();			
		} finally {
			try {
			conn.close();
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
										
		return rsListRet;		
	}	
	
	public List<Map<String,Object>> getResultList(String strSqlStmt, Map<Integer,Object> mapParam) {

		Connection conn = null;
		List<Map<String,Object>> rsListRet = new ArrayList<Map<String,Object>>();
		
		try {
			conn = ds.getConnection();
			
			PreparedStatement prepStmt = conn.prepareStatement(strSqlStmt);		
						
			for(Integer i: mapParam.keySet()) {
				prepStmt.setObject(i, mapParam.get(i));
			}
			
			ResultSet rs = prepStmt.executeQuery();
			ResultSetMetaData rsmd = rs.getMetaData();
			
			while(rs.next()) {
				
				HashMap<String,Object> map = new LinkedHashMap<String,Object>();
									
				for (int i = 1; i <= rsmd.getColumnCount(); i++) {										
					map.put(rsmd.getColumnName(i), rs.getObject(i));					
				}
				rsListRet.add(map);								
			}		
			
		} catch(Exception e) {
			e.printStackTrace();			
		} finally {
			try {
				conn.close();
			} catch(Exception e) {
				e.printStackTrace();
			}
		}		
		
		return rsListRet;		
	}	
	
	public int execUpdate(String strSqlStmt, Map<Integer,Object> mapParam) {
		
		int nUpdateCount = 0;
		
		Connection conn = null;
		
		try {
			conn = ds.getConnection();
			
			PreparedStatement prepStmt = conn.prepareStatement(strSqlStmt);		
						
			for(Integer i: mapParam.keySet()) {
				prepStmt.setObject(i, mapParam.get(i));
			}		
			
			nUpdateCount = prepStmt.executeUpdate();
			
		} catch(Exception e) {
			e.printStackTrace();			
		} finally {
			try {
				conn.close();
			} catch(Exception e) {
				e.printStackTrace();
			}
		}	
		
		return nUpdateCount;
	}

}
